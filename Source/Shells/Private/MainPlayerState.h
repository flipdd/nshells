#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MainPlayerState.generated.h"

/**
 *
 */
UCLASS()
class SHELLS_API AMainPlayerState : public APlayerState
{
	GENERATED_BODY()

private:
	class AHTTPService* HTTPServiceRef;

protected:
	UPROPERTY(Replicated)
	FString AuthenticationHash;

	UPROPERTY(Replicated)
		int PID;
	UPROPERTY(Replicated)
		int TotalPoints;
	UPROPERTY(Replicated)
		int TotalDeaths;
	
	// 1 means , 2 means, 3 means ...-
	//UPROPERTY(Replicated)
	//	int ModifierItem;

public:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	void InitPlayerSession(AHTTPService* HTTPService, FString hash, int id, int points, int deaths);

	void AddDeath(int score);
	void AddKill(int score);
	void AddScore(int score);

	UFUNCTION(BlueprintCallable)
	void AddPlayerPoint();

	UFUNCTION(BlueprintCallable)
	void AddPlayerDeath();

	int GetScore() const { return Score; };
	int GetKills() const { return Kills; };
	int GetDeaths() const { return Deaths; };

private:
	int Score;
	int Deaths;
	int Kills;
};
