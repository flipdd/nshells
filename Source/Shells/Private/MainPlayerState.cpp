
#include "MainPlayerState.h"
#include "UnrealNetwork.h"
#include "Session\HTTPService.h"

void AMainPlayerState::InitPlayerSession(AHTTPService* HTTPService,
	FString hash, int id, int points, int deaths) {
	HTTPServiceRef = HTTPService;
	AuthenticationHash = hash;
	PID = id;
	TotalPoints = points;
	TotalDeaths = deaths;
	UE_LOG(LogTemp, Warning, TEXT("Hash is /%s"), *hash);
	FString http = HTTPServiceRef->GetName();
	UE_LOG(LogTemp, Warning, TEXT("http name is: %s"), *http);
}

void AMainPlayerState::AddPlayerPoint()
{
	TotalPoints++;

	UE_LOG(LogTemp, Warning, TEXT("Added point"));

	if (HTTPServiceRef)
	{
		FAddPointRequest AddPointInfo;
		AddPointInfo.hash = AuthenticationHash;
		AddPointInfo.userid = FString::FromInt(PID);
		HTTPServiceRef->AddPoint(AddPointInfo);

		UE_LOG(LogTemp, Warning, TEXT("Add point info id : %d , hash %s"), *AddPointInfo.userid, *AddPointInfo.hash);
	}
}

void AMainPlayerState::AddPlayerDeath()
{
	TotalDeaths++;

	UE_LOG(LogTemp, Warning, TEXT("Added death"));

	if (HTTPServiceRef)
	{
		FAddDeathRequest AddDeathInfo;
		AddDeathInfo.hash = AuthenticationHash;
		AddDeathInfo.userid = FString::FromInt(PID);
		HTTPServiceRef->AddDeath(AddDeathInfo);

		UE_LOG(LogTemp, Warning, TEXT("Add point info id : %d , hash %s"), *AddDeathInfo.userid, *AddDeathInfo.hash);
	}
}

void AMainPlayerState::GetLifetimeReplicatedProps(TArray<
	FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(AMainPlayerState, AuthenticationHash);
	DOREPLIFETIME(AMainPlayerState, TotalPoints);
	DOREPLIFETIME(AMainPlayerState, TotalDeaths);
	DOREPLIFETIME(AMainPlayerState, PID);
}