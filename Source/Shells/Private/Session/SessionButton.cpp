// Fill out your copyright notice in the Description page of Project Settings.


#include "SessionButton.h"
#include "MatchmakingLevelScript.h"

USessionButton::USessionButton()
{
    OnClicked.AddDynamic(this, &USessionButton::OnClick);
}

void USessionButton::OnClick()
{
	if (GetOuter()->GetWorld()) {
		if (auto Controller = GetOuter()->GetWorld()->GetFirstPlayerController()) {
            const FString Cmd = "open " + SessionInfo->ServerIp + ":" + FString::FromInt(SessionInfo->ServerPort);
			Controller->ConsoleCommand(Cmd);
		}
	}
}
void USessionButton::SetSessionInfo(FSessionInfo *Info)
{
    SessionInfo = Info;
}