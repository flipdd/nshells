
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/public/Http.h"
#include "Json.h"
#include "JsonUtilities.h"
#include "HTTPService.generated.h"

USTRUCT()
struct FLoginRequest 
{
	GENERATED_BODY()

	UPROPERTY()
		FString email;
	UPROPERTY()
		FString	password;
};

USTRUCT()
struct FLoginData
{
	GENERATED_BODY()

	UPROPERTY()
		int status;
	UPROPERTY()
		int userid;
	UPROPERTY()
		FString name;
	UPROPERTY()
		FString hash;
	UPROPERTY()
		int totalpoints;
	UPROPERTY()
		int totaldeaths;
};

USTRUCT()
struct FLoginResult
{
	GENERATED_BODY()

	UPROPERTY()
		FLoginData LoginResult;
};

USTRUCT()
struct FAddPointRequest
{
	GENERATED_BODY()

	UPROPERTY()
		FString userid;
	UPROPERTY()
		FString hash;
};

USTRUCT()
struct FAddPointData
{
	GENERATED_BODY()

	UPROPERTY()
		int status;
};

USTRUCT()
struct FResponseAddPoint
{
	GENERATED_BODY();

	UPROPERTY()
		FAddPointData AddUserPointResult;
};

USTRUCT()
struct FAddDeathRequest
{
	GENERATED_BODY()

	UPROPERTY()
		FString userid;
	UPROPERTY()
		FString hash;
};

USTRUCT()
struct FAddDeathData
{
	GENERATED_BODY()

	UPROPERTY()
		int status;
};

USTRUCT()
struct FResponseAddDeath
{
	GENERATED_BODY();

	UPROPERTY()
		FAddDeathData AddUserDeathResult;
};
 
UCLASS()
class SHELLS_API AHTTPService : public AActor
{
	GENERATED_BODY()

public:
	AHTTPService();

	void Tick(float DeltaTime) override;

protected:
	void BeginPlay() override;

	FHttpModule* Http;

	FString APIBaseURL;

	TSharedRef<IHttpRequest> RequestWithRoute(FString Subroute);
	TSharedRef<IHttpRequest> GetRequest(FString Subroute);
	TSharedRef<IHttpRequest> PostRequest(FString Subroute, FString ContentJsonString);
	void Send(TSharedRef<IHttpRequest> Request);
	bool ResponseIsValid(FHttpResponsePtr Response, bool WasSuccessful);

	class ACharacter* LocalCharacter;

public:

	UPROPERTY(EditDefaultsOnly, Category = "UI")
		TSubclassOf<class UUserWidget> LoginWidgetClass;

	class UUserWidget* LoginWidget;

	void Login(FLoginRequest LoginCredentials, class APlayerState* playerState);

	void LoginResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful, APlayerState* playerState);

	UFUNCTION()
		void OnLoginClicked();

	void AddPoint(FAddPointRequest AddPointInfo);

	void AddPointResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	void AddDeath(FAddDeathRequest AddDeathInfo);

	void AddDeathResponse(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);
};
