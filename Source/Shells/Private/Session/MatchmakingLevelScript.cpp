
#include "MatchmakingLevelScript.h"

#include "Button.h"
#include "VerticalBox.h"
#include "TextBlock.h"
#include "TimerManager.h"
#include "SessionButton.h"
#include "VerticalBoxSlot.h"
#include "ScrollBox.h"
#include "UObject/ConstructorHelpers.h"

#include "UI/MatchmakingWidget.h"

AMatchmakingLevelScript::AMatchmakingLevelScript()
{
	ConstructorHelpers::FClassFinder<UUserWidget> MatchMakingBPClass(TEXT("/Game/Blueprints/Widgets/WBP_Matchmaking.WBP_Matchmaking_C"));
	if (!ensure(MatchMakingBPClass.Class != nullptr)) return;
	MatchmakingWidgetClass = MatchMakingBPClass.Class;
}

void AMatchmakingLevelScript::BeginPlay()
{
	Super::BeginPlay();

	bReadyToHost = false;

	GetWorld()->GetTimerManager().SetTimer(ServerListTimerHandle, this, &AMatchmakingLevelScript::OnUpdateServerList, 2, true);

    if (MatchmakingWidgetClass) {
		MatchmakingWidget = CreateWidget<UUserWidget>(GetWorld(), MatchmakingWidgetClass);
		MatchmakingWidget->AddToViewport();

        auto ConnectButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("ConnectButton")));
		if (ConnectButton) {
		    ConnectButton->OnClicked.AddDynamic(this, &AMatchmakingLevelScript::OnConnectClicked);
		}

        auto HostButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("HostButton")));
		if (HostButton) {
			HostButton->SetIsEnabled(false);
			HostButton->OnClicked.AddDynamic(this, &AMatchmakingLevelScript::OnHostClicked);
		}

		ServerListScrollBoxWidget = Cast<UScrollBox>(MatchmakingWidget->GetWidgetFromName(TEXT("MyScrollBox")));
	}

    const auto Controller = GetWorld()->GetFirstPlayerController();
	if (Controller) {
		Controller->bShowMouseCursor = true;
		Controller->bEnableClickEvents = true;
		Controller->bEnableMouseOverEvents = true;
	}
}

void AMatchmakingLevelScript::StartGameHost(int Port)
{
	HostPort = Port;
	bReadyToHost = true;
}


void AMatchmakingLevelScript::UpdateSessionsList(FString ServerInfo)
{
	TArray<FString> Out;
	ServerInfo.ParseIntoArray(Out, TEXT("|"), true);
	for (int i = 1; i < Out.Num() - 3; i += 4) {
		FSessionInfo TempInfo;
		TempInfo.ID = FCString::Atoi(*Out[i]);
		TempInfo.Name = Out[i + 1];
		TempInfo.ServerIp = Out[i + 2];
		TempInfo.ServerPort = FCString::Atoi(*Out[i + 3]);
		ServersList.Add(TempInfo);
	}
}

void AMatchmakingLevelScript::OnConnectClicked()
{
    TcpClient = new TCPClient(this);
}

void AMatchmakingLevelScript::OnHostClicked()
{
    TcpClient->HostNewGame("My test server", "8804");
}

void AMatchmakingLevelScript::OnUpdateServerList()
{
	if (TcpClient) {
		if (TcpClient->IsConnected()) {
            auto HostButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("HostButton")));
			HostButton->SetIsEnabled(true);
			if (bReadyToHost) {
				if (auto Controller = GetWorld()->GetFirstPlayerController()) {
				    Controller->ConsoleCommand("open MyMap?listen");
				}
			}
		}
		if (ServersList.Num() > 0) {
			if ((MatchmakingWidgetClass) && (MatchmakingWidget) && (ServerListScrollBoxWidget)) {
                auto AllChildren = ServerListScrollBoxWidget->GetAllChildren();

			    for (int i = 0; i < AllChildren.Num(); i++) {
			        AllChildren[i]->RemoveFromParent();
			    }

				for (int i = 0; i < ServersList.Num(); i++) {
                    auto ItemWidgetsBox = NewObject<UVerticalBox>();
					ServerListScrollBoxWidget->AddChild(ItemWidgetsBox);

                    auto ItemWidget = NewObject<USessionButton>(this);
					ItemWidget->SetSessionInfo(&ServersList[i]);

				    auto ItemWidgetText = NewObject<UTextBlock>();
					ItemWidgetText->SetText(FText::FromString(ServersList[i].Name));
					ItemWidget->AddChild(ItemWidgetText);

				    auto Slot = ItemWidgetsBox->AddChildToVerticalBox(ItemWidget);
					Slot->SetPadding(FMargin {5});
				}
			}
		}
	}
}