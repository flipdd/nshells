// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "Session/TCPClient.h"

#include "UI/MatchmakingWidget.h"
#include "MatchmakingLevelScript.generated.h"

USTRUCT()
struct FSessionInfo
{
	GENERATED_BODY();

	UPROPERTY() int ID;
	UPROPERTY() FString Name;
	UPROPERTY() FString ServerIp;
	UPROPERTY() int ServerPort;
};
/**
 *
 */
UCLASS()
class AMatchmakingLevelScript : public ALevelScriptActor
{
	GENERATED_BODY()

public:
	AMatchmakingLevelScript();
	void UpdateSessionsList(FString ServerInfo);
	void StartGameHost(int Port);

protected:
	void BeginPlay() override;

	TArray<FSessionInfo> ServersList;
	TCPClient *TcpClient;

	bool bReadyToHost;
	int HostPort;

	TSubclassOf<class UUserWidget> MatchmakingWidgetClass;

    UUserWidget *MatchmakingWidget;
	class UScrollBox *ServerListScrollBoxWidget;
	FTimerHandle ServerListTimerHandle;

    UFUNCTION()
		void OnUpdateServerList();
	UFUNCTION()
		void OnConnectClicked();
	UFUNCTION()
		void OnHostClicked();
};
