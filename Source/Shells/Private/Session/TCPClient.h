// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runnable.h"
#include "Sockets.h"

class SHELLS_API TCPClient : public FRunnable
{
public:
	explicit TCPClient(class AMatchmakingLevelScript *LevelScript);
	~TCPClient();

    bool Init() override;
    uint32 Run() override; 
    void Stop() override;

    void HostNewGame(FString Name, FString Port) const;
	bool IsConnected() const;

private:
	FRunnableThread *Thread;
	FSocket *Socket;
	FSocket *ListenerSocket;

    bool bIsRunning;
	bool bIsConnected;
	class AMatchmakingLevelScript *GameLevel;
};